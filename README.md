![Terraform](https://lgallardo.com/images/terraform.jpg)

# tf-aws-xxx
---
This module manage AWS .. [terraform-module-aws-xxx].

## MODULE

```hcl
module "module_xxx" {

  #source = "git::<url>?ref=<tag>"
  source  = "git::https://gitlab.com/cloud7392819/aws1872017/tf-modules/tf-aws-xxx.git?ref=tags/1.0.x"
  #source = "./modules/module_xxx"

  ##################################################################
  # Common/General settings
  ##################################################################

  # Whether to create the resources (`false` prevents the module from creating any resources).
  module_enabled = false

  # Emulation of `depends_on` behavior for the module.
  # Non zero length string can be used to have current module wait for the specified resource.
  module_depends_on = ""

  # Application name
  name   = var.cut_name

  # Additional mapping of tags to assign to the all linked resources.
  module_tags = {
    ManagedBy   = "Terraform"
  }
}
```

## Usage

To run this example you need to execute:

```bash
$ terraform init
$ terraform plan
$ terraform apply
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 5 |
| <a name="requirement_random"></a> [random](#requirement\_random) | >= 2.1 |

## Providers

No providers.

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 5 |
| <a name="provider_random"></a> [random](#provider\_random) | >= 2.1 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_xxx"></a> [cache\_bucket](#module\_cache\_bucket) | git::https://gitlab.com/xxxx | 6.0.0 |


## Resources

No resources.

| Name | Type |
|------|------|

| [aws_iam_policy.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.default_cache_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="module_enable"></a> [module\_enable](#input\_module\_enable) | Whether to create the resources | `bool` | `false` | true |
| <a name="module_depend_on"></a> [module\_depend\_on](#input\_module\_depend\_on) | Emulation of behavior for the module. | `string` | `""` | no |
| <a name="name"></a> [name](#input\_name) | Application name | `string` | `""` | name |
| <a name="module_tags"></a> [module\_tags](#input\_module\_tags) | Additional mapping of tags to assign to the all linked resources | `map(string)` | `{}` | no |

## Outputs

No outputs.

| Name | Description |
|------|-------------|
| <a name="output_project_name"></a> [project\_name](#output\_project\_name) | Project name |
| <a name="output_role_arn"></a> [role\_arn](#output\_role\_arn) | IAM Role ARN |
| <a name="output_role_id"></a> [role\_id](#output\_role\_id) | IAM Role ID |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Versioning
This repository follows [Semantic Versioning 2.0.0](https://semver.org/)

## Git Hooks
This repository uses [pre-commit](https://pre-commit.com/) hooks.

## URLs

1.
2.


##